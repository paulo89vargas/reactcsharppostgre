﻿namespace APIC.Models
{
    public class Estado
    {
        public string Id { get; set; }
        public string Sigla { get; set; }
        public string Nome { get; set; }
    }
}
