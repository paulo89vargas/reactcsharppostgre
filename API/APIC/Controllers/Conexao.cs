﻿using Npgsql;


namespace APIC.Controllers
{
    public class Conexao
    {
        public static NpgsqlConnection GetConexao()
        {
            {
                NpgsqlConnection conexao = null;
                try
                {
                    conexao = new NpgsqlConnection("Server=localhost;Port=5432;User Id=postgres;Password=379229;Database=financeiro;");
                    conexao.Open();
                }catch(Exception e)
                { 
                    Console.WriteLine("Erro de conexão: " + e.Message);
                }
                return conexao;
            }

            static void SetFechaConexao(NpgsqlConnection conexao)
            {
                if(conexao != null)
                {
                    try
                    {
                        conexao.Close();
                    }catch(Exception e)
                    {
                        Console.WriteLine("Erro ao fechar conexão: " + e.Message);
                    }
                }
            }
        }        
    }
}
