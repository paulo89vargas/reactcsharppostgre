﻿using APIC.Models;
using Microsoft.AspNetCore.Mvc;

namespace APIC.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EstadoController : Controller
    {
        public static List<Estado> lista = new List<Estado>();

        [HttpGet]
        public List<Estado> GetEstados()
        {
            List<Estado> lista = EstadoDB.GetEstados();
            return lista;
        }

        [HttpPost]
        public string PostEstado(Estado estado)
        { 
            bool result = EstadoDB.IncluiEstado(estado);
            if (result)
            {
                return "Estado cadastrado com sucesso!";
            }
            else
            {
                return "Erro ao cadastrar o estado!";
            }            
        }

        [HttpPut]
        public string PutEstado(Estado estado)
        {
            bool result = EstadoDB.AlteraEstado(estado);
            if (result)
            {
                return "Estado alterado com sucesso!";
            }
            else
            {
                return "Erro ao alterar o estado!";
            }
        }

        [HttpDelete]
        public string DeleteEstado(Estado estado)
        {
            bool result = EstadoDB.ExcluiEstado(estado);
            if (result)
            {
                return "Estado excluido com sucesso!";
            }
            else
            {
                return "Erro ao excluir o estado!";
            }
        }
    }
}
