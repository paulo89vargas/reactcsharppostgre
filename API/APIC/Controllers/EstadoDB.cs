﻿using APIC.Models;
using Npgsql;

namespace APIC.Controllers
{
    public class EstadoDB
    {
        public static List<Estado> GetEstados()
        {
            List<Estado> lista  = new List<Estado>();
            try
            {
                NpgsqlConnection conexao = Conexao.GetConexao();
                string sql = "select * from estado order by id";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, conexao);
                NpgsqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    string id = dr["id"].ToString();
                    string sigla = dr["est_sigla"].ToString();
                    string nome = dr["nome"].ToString();
                    Estado estado = new Estado();
                    estado.Id = id;
                    estado.Sigla = sigla;
                    estado.Nome = nome;
                    lista.Add(estado);
                }

            }catch(Exception e)
            {
                Console.WriteLine("Erro de SQL: "+ e.Message);
            }
            return lista;
        }

        public static bool IncluiEstado(Estado estado)
        {
            bool result = false;
            try
            {
                NpgsqlConnection conexao = Conexao.GetConexao();
                string sql = "insert into estado (est_sigla,nome) values(@sigla,@nome)";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, conexao);
                cmd.Parameters.Add("@sigla", NpgsqlTypes.NpgsqlDbType.Varchar).Value = estado.Sigla;
                cmd.Parameters.Add("@nome", NpgsqlTypes.NpgsqlDbType.Varchar).Value = estado.Nome;
                int valor = cmd.ExecuteNonQuery();
                if (valor > 0)
                {
                    result = true;
                }
            }
            catch(Exception e)
            {
                Console.WriteLine("Erro de SQL:" + e.Message);
            }
            return result;
        }

        public static bool AlteraEstado(Estado estado)
        {
            bool result = false;
            try
            {
                NpgsqlConnection conexao = Conexao.GetConexao();
                string sql = "update estado set nome = @nome where est_sigla = @sigla";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, conexao);
                cmd.Parameters.Add("@sigla", NpgsqlTypes.NpgsqlDbType.Varchar).Value = estado.Sigla;
                cmd.Parameters.Add("@nome", NpgsqlTypes.NpgsqlDbType.Varchar).Value = estado.Nome;
                int valor = cmd.ExecuteNonQuery();
                if (valor > 0)
                {
                    result = true;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Erro de SQL:" + e.Message);
            }
            return result;
        }
        public static bool ExcluiEstado(Estado estado)
        {
            bool result = false;
            try
            {
                NpgsqlConnection conexao = Conexao.GetConexao();
                string sql = "delete from estado where est_sigla = @sigla";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, conexao);
                cmd.Parameters.Add("@sigla", NpgsqlTypes.NpgsqlDbType.Varchar).Value = estado.Sigla;
                int valor = cmd.ExecuteNonQuery();
                if (valor > 0)
                {
                    result = true;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Erro de SQL:" + e.Message);
            }
            return result;
        }
    }
}
